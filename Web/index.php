               
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ultrasoniiic!</title>
		<style type="text/css">
			.glass {
			    margin: 30px auto;
			    height: 300px;
			    width: 600px;
			    position: relative;
			    border-style: none solid solid solid;
			    border-width: 10px;
			    border-color: lightgray;
			    border-radius: 10px;
			}

			.water {
			    width: 100%;
			    height: 10%;
			    background-color: skyblue;
			    position: absolute;
			    bottom: 0;
			}
		</style>
	</head>
	<body style="font-size: 5em">


	<div style="text-align: center;">
		<h1 style="cursor: pointer;">Lotan</h1>
		<div class="glass">
    		<div class="water"></div>
		</div>
		<div id="content" style="font-family: monospace;"></div>


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script type="text/javascript">

	    document.onload = pageReload();
	    var containerStatus;
	    var loadedContent;
	    var percentStatus = 0;

	    function pageReload()
	    {
	        $.get("pullArduino.php", function(loadedContent){
	        	containerStatus = loadedContent;
	        });

	        if(containerStatus >= 450 && containerStatus <= 900)
	        {
	        	phasedStatus = containerStatus - 450;

	        	if(phasedStatus != 0)
	        	{
	        		percentStatus = 100 - ((phasedStatus * 100) / 450);
	        	}
	        	else
	        	{
	        		percentStatus = 100;
	        	}
	        }

	        //$('#content').html(percentStatus+'%');
		    $('.water').animate({
		        height: percentStatus+'%'
		    }, 100)

	        var del = setTimeout(pageReload, 150)   
	    }

	</script>

	

	</body>
</html>
